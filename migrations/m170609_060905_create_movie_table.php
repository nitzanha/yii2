<?php

use yii\db\Migration;

/**
 * Handles the creation of table `movie`.
 */
class m170609_060905_create_movie_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
			     $this->createTable('movie', [
            'id' => $this->primaryKey(),
			'name' => $this->string()->notNull(),
			'genre' => $this->string()->notNull(),
			'min_age' => $this->string()->notNull(),
			'grade' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('movie');
    }
}
