<?php

use yii\db\Migration;

/**
 * Handles the creation of table `student`.
 */
class m170526_150818_create_student_table extends Migration
{
    /**
     * @inheritdoc
     */
   public function up()
    {
        $this->createTable('student', [
            'id' => $this->primaryKey(),
			'name' => $this->string()->notNull(),
			'age' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('student');
    }
}
