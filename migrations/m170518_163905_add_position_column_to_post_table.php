<?php

use yii\db\Migration;

/**
 * Handles adding position to table `post`.
 */
class m170518_163905_add_position_column_to_post_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
		$this->addColumn('customers', 'description', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
		$this->dropTable('customers');
    }
}
