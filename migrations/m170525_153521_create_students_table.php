<?php

use yii\db\Migration;

/**
 * Handles the creation of table `students`.
 */
class m170525_153521_create_students_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('students', [
            'id' => $this->primaryKey(),
			'name' => $this->string()->notNull(),
			'age' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('students');
    }
}
