<?php

use yii\db\Migration;

/**
 * Handles the creation of table `abc`.
 */
class m170529_152114_create_abc_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('abc', [
            'id' => $this->primaryKey(),
			'name' => $this->string()->notNull(),
			'age' => $this->integer()->notNull(),
        ]);

		
		
		
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('abc');
    }
}
