<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Abc;

class AbcController extends Controller
{
 
	public function actionView($id)
    {
		$student = Abc::getStudent($id);
		return $this->render('view',['student' => $student]);
    }
	
	public function actionViewall()
	{
		$student = Abc::getStudents();
		return $this->render('viewAll',['student' => $student]);
	}
}
